// ==UserScript==
// @name         Плюшки для КШП.бел
// @namespace    http://tampermonkey.net/
// @description  Повышает удобство работы администратора с кшп.бел
// @version      3.4
// @author       Dmitrii Stanski
// @match        https://ipay.znaj.by/*
// @grant        none
// ==/UserScript==
$.prototype.indexOf=function(selector){
	for(var i = 0; i < this.length; i++){
		if($(this[i]).is(selector)){
			return i;
        }
	}
};
function countPortions(num){
    var groups = [];
                $(`#table_add td:nth-child(${num+3})`).each(function () {
                    var i = (this.innerText);
                    if(i==="0,00")
                        return;
                    if (groups[i] === undefined)
                        groups[i] = 0;
                    groups[i]++;
                });
                //groups.sort(function (a,b) { return a - b; })
                //console.log(groups);
                var strs = [];
                for (let propName in groups) {
                    if (groups.hasOwnProperty(propName)) {
                        const propValue = groups[propName];
                        strs.push(`${propName}: ${propValue}`);
                    }
                }
                var str = strs.join("<br>");
                console.log(str);
                $("#result"+num).html(str);
                $("#className").html($(".class-info > span:nth-child(2) span").text());
}
function observ(target, onChildList, attrs, children) {
    var targetNode = $(target)[0];
    if (targetNode === undefined)
        return undefined;
    // Options for the observer (which mutations to observe)
    var config = { attributes: attrs, childList: children, subtree: false };

    // Callback function to execute when mutations are observed
    var callback = function (mutationsList) {
        for(var mutation of mutationsList) {
            if (mutation.type === "childList") {
                console.log("A child node has been added or removed.");
                onChildList(mutation);
            }
            else if (mutation.type === "attributes") {
                console.log(`The ${mutation.attributeName} attribute was modified.`);
                onChildList(mutation);
            }
        }
    };

    // Create an observer instance linked to the callback function
    var observer = new MutationObserver(callback);

    // Start observing the target node for configured mutations
    observer.observe(targetNode, config);
    return observer;
}

// возвращает cookie с именем name, если есть, если нет, то undefined
function getCookie(name) {
    var matches = document.cookie.match(new RegExp(
      `(?:^|; )${name.replace(/([\.$?*|{}\(\)\[\]\\\/\+^])/g, "\\$1")}=([^;]*)`
    ));
    return matches ? decodeURIComponent(matches[1]) : undefined;
}

//https://learn.javascript.ru/cookie
function setCookie(name, value) {
    var tomorrow = new Date();
    tomorrow.setDate(tomorrow.getDate() + 2);
    tomorrow = new Date(`${(tomorrow.getMonth() + 1)},${tomorrow.getDate()},${tomorrow.getFullYear()},00:00:00`);
    const options = { path: "/", expires: tomorrow };

    const expires = options.expires;

    if (expires && expires.toUTCString) {
        options.expires = expires.toUTCString();
    }

    value = encodeURIComponent(value);

    var updatedCookie = `${name}=${value}`;

    for (let propName in options) {
        if (options.hasOwnProperty(propName)) {
            updatedCookie += `; ${propName}`;
            const propValue = options[propName];
            if (propValue !== true) {
                updatedCookie += `=${propValue}`;
            }
        }
    }

    document.cookie = updatedCookie;
}

function Menu(date) {
    if (date === undefined) {
        console.error("undefined date!");
    }
    else
        this.date = date;
}

Menu.prototype = Object.create(Array.prototype);
Menu.prototype.constructor = Menu;

Menu.load = function (date) {
    var c = getCookie("menu" + date);
    if (c === undefined) {
        return new Menu(date);
    } else {
        var jmenu = window.JSON.parse(c);
        if (jmenu !== undefined) {
            var menu = new Menu(date);
            for (var i = 0; i < jmenu.length; i++) {
                menu.push(jmenu[i]);
            }
            return menu;
        }
    }
}

Menu.prototype.save = function () {
    setCookie("menu" + this.date, window.JSON.stringify(window.menu));
};

function getMenu(date) {
    if (date === undefined)
        date = $('caption').text().match(/\d+\.\d+.\d+/)[0];
    if (window.menu === undefined || (window.menu.date !== undefined && window.menu.date !== date)) {
        window.menu = Menu.load(date);
    }

    return window.menu;
}

function setModsKeyStates(e) {
    window.lastCtrlState = e.ctrlKey;
    window.lastShiftState = e.shiftKey;
    window.lastAltState = e.altKey;
}

function complexToBtn(complex) {
    var btn = $(`<div class=\"button-design-02 icon_close complex-btn\">${complex.name}</div>`);
    return btn.click(function (e) {
        if (e.ctrlKey == true) {
            console.log(this);
            let menu = getMenu();
            let i = menu.indexOf(this);
            menu.splice(i, 1);
            $(e.toElement).remove();
            menu.save();
            updateComplexSelector();
            return;
        } else {
            setComplex(this);
        }
    }.bind(complex));
}

function setComplex(complex) {
    switch (complex.mode) {
        case "set":
            $("input[id*=menu_]").val(0);
            $(complex.set).each(function () {
                $("#" + this.id).val(this.val);
            });
            break;
        case "add":
            $(complex.set).each(function () {
                let input = $("#" + this.id);
                input.val(input.val() + this.val);
            });
            break;
        case "del":
            $(complex.set).each(function () {
                let input = $("#" + this.id);
                input.val(input.val() - this.val);
            });
            break;

        default:
            {
                console.error("invalid set mode");
                return;
            }
    }
    $("input.update_pupil_btn").click();
}

function complexToInput(complex) {
    var inp = $('<input type="radio" name="complex_selector"/>');
    inp[0].complex = complex;
    return $(`<label><span>${complex.name}</span></label>`).prepend(inp);
}
function addCheckBox(name,caption){
    $("#additionBox").append("<br>");
    $("#additionBox").append($(`<label><span>${caption}</span></label>`).prepend($(`<input type='checkbox' id='is${name}Box'>`).click(function(){
	setCookie(name, this.checked);})));    
    $(`#is${name}Box`).prop("checked",getCookie(name)=="true");
}
function updateComplexSelector() {
    var complexSelector = $("#complexSelector");
    if (complexSelector.length == 0) {
        $('<div id="complexSelector"></div>')
        .appendTo($(`<div id="additionBox" class="content"><b>(<span id="className"></span>) Порции:</b><p><b>Завтрак</b></p><p id="result1"/><p><b>Обед</b></p><p id="result2"/><p><b>Полдник</b></p><p id="result3"/><p><b>Инструкция:</b><br>Ctrl+Кнопка - очистка;<br>Shift+Кнопка - 1ое в списке<br>Alt+Кнопка - Выбранное</p><b>Режимы Alt:</b></div>`).insertAfter("#short_info_block"));
        addCheckBox("night","Ночной режим");
        addCheckBox("fancy","Прописной шрифт");
        addCheckBox("mono","Моноширинный шрифт");
    } else {
        complexSelector.html("");
    }

    let menu = getMenu();
    for (let complex of menu) {
        let menu = getMenu();
        complexToInput(complex)
        .appendTo(complexSelector)
        .find("input")
        .prop('checked', complex.checked)
        .click(function () {
            this.complex.checked = true;
            $("#complexSelector input:not(:checked)").each(function () { this.complex.checked = false; });
            getMenu().save();
        });
    }
}

(function () {
    "use strict";
    /******** Инициализация *********/

    window.onbeforeunload = function (evt) {
        setCookie("curc", $(".oak__leaf").indexOf(".selected"));
        setCookie("curp", $(".kvp-menu").indexOf(".selected"));
        return null;
    }
    window.onbeforeunload;
    setTimeout(function run(){
        var time= $(".logout-timer")[0].innerText.split(":")[0];
        if(time=="01"){
            window.location.reload();
            return;
        }
        setTimeout(run,1000);
    },1000);

    // переход к странице
    var c = 0;
    var p = 5;
    var curc =getCookie("curc");
    var curp =getCookie("curp");
    if(curc!=="undefined"){
        c = parseInt(curc);
    }
    if(curp!=="undefined"){
        p = parseInt(curp);
    }
    $(`.oak__leaf a`).get(c).click();
    $(`.kvp-menu a`).get(p).click();

    // Показать меню и заблокировать клик
    $(".oak__branch").unbind("click").find("ul").show();
    
    /************************            СТИЛЬ            *************************/
    // стиль
    $("<style>#complexSelector input[type='radio'], #cmode input[type='radio'] { opacity: 100 !important;  position: initial !important;}" 
      + (getCookie("night")=="true"?"body{background:#040404}.row .content,table,tr,header,header *,.popup{background:#202521 !important;border-radius:8px;color:#8e8e8e !important}":"") 
      + "td:last-child i{margin-right:3px}form#form_modal>*{display:inline-block;vertical-align:top}.popup_footer>*{display:block}.popup.popupIpay{width:auto}.content.myinput>*{width:160px;padding:10px;margin:4px}.content.myinput>label{font-weight:700}.popup.popupIpay.popupLarge{width:auto !important}#menu_name{margin:5px 0 0 15px}.popup_footer.editPupilPopupFooter *[class*=button]{margin:0;width:100%;margin:0px 0px 6px 0px}#additionBox{position:fixed;top:6%;background:white;right:0;z-index:999;border:1px solid}#additionBox input{margin:0 3px 0px 16px}#complexSelector label{display:block}#add_complex{margin:7px 0px 7px 0px;padding:10px;border:2px dashed #dadada;border-radius:3px}#cmode input{margin:-7px 5px 0px 0px;vertical-align:middle}li ul > li, #table_add tr td{transition:all 0.2s linear}li ul>li{padding:1px 9px}li ul > li:hover, #table_add tr:hover td{background:#11c263}#table_add tr.setn:hover td{color:#ff0}#className{color:red}"
      + (getCookie("fancy")=="true"?'*{font-family: "Segoe Script";}':"")
      + (getCookie("mono")=="true"?'*{font-family: MONOSPACE;}':"")
      + "</style>").appendTo("body");
    // отлов ctrl
    window.lastCtrlState = false;
    window.lastShiftState = false;
    window.lastAltState = false;

    $(document)[0].addEventListener("keydown", setModsKeyStates, true);
    $(document)[0].addEventListener("keyup", setModsKeyStates, true);

    /*
     * Интеграция в диалоговоые окна
     * (реакция на изменения)
     */
    var obs1 = observ(".popup_wrap",
        function (m) {
            if ((/Добавить ученика/).test($(".popup_name").text())) {
                var fill = function () {
                    if(window.List.cur<window.List.length){
                        $('#surname').val(window.List[window.List.cur][0]);
                        $('#firstname').val(window.List[window.List.cur][1]);
                        $('#patronymic').val(window.List[window.List.cur][2]);
                        $('#birthday').val(window.List[window.List.cur][3].replace(/(\d+).(\d+).(\d+)/,"$3-$2-$1"));
                        window.List.cur++;
                    }
                    else{
                        window.List = undefined;
                    }
                }
                if(window.List == undefined){                    
                    $('body').append("<div id='inputBox' class='popup_wrap active'><div class='popup popupIpay'><div class='popup_header'><div class='popup_name'>Вставьте учеников из excel</div></div><div class='popup_content'><textarea style='height:200px'></textarea></div><div class='popup_footer '><div class='button-design-02 icon_close' onclick='$(\"#inputBox\").remove()'>Отменить</div></div></div></div>");
                    $('#inputBox .popup_footer').append($("<input class='button-design-01 update_pupil_btn' value='Выполнить'>").click(function () {
                        let arr =[];
                        let text = $("#inputBox textarea").val();
                        let expr = /([а-яА-Я]+)\t+([а-яА-Я]+)\t+([а-яА-Я]+)\t+([0-9.]+)\n/g;
                        var res;
                        while((res = expr.exec(text))!==null){
                            arr.push([res[1],res[2],res[3],res[4]]);
                        }
                        $("#inputBox").remove();
                        arr.cur = 0;
                        if(arr.length>0){ 
                            window.List = arr;
                        }
                        fill();
                    }));
                }
                else {
                    fill();
                }
            }
           else if ((/Редактировать класс/).test($(".popup_name").text())) {
                $('<input type="submit" class="button-design-01" value="Перевести на год">').click(function () {
                    $(".pupilMainInfo #name").val($(".pupilMainInfo #name").val().replace(/\d+/, function (m2) {
                        return ++m2;
                    }));
                    $(".pupilMainInfo #year").val($(".pupilMainInfo #year").val().replace(/\d+/, function (m2) {
                        return ++m2;
                    }));
                    $(".pupilMainInfo #classno").val($(".pupilMainInfo #classno").val().replace(/\d+/, function (m2) {
                        return ++m2;
                    }));
                    console.log("done");
                }).appendTo(".editPupilPopupFooter");
            } else if ((/установить "Н"/).test($(".popup_name").text())) {
                $("input.update_pupil_btn").click();
            }
            else
                if ((/^Ведомость/i).test($("#table caption").text())) {
                    if ($(".popup_wrap").hasClass("active") && (/^Выбор/i).test($(".popup_name").text())) {
                        if (window.lastCtrlState) {
                            $("#modal_id .editPupilPopupFooter > *").filter(function () {
                                return (/Сбросить/i).test($(this).text());
                            }).click();
                            $("#modal_id input.update_pupil_btn").click();
                            return;
                        }
                        if (window.lastShiftState) {
                            setComplex(getMenu()[0]);
                            return;
                        }
                        if (window.lastAltState) {
                            setComplex($("#complexSelector input:checked")[0].complex);
                            return;
                        }

                        var menu = getMenu();
                        $("#add_complex").remove();
                        $(".complex-btn").remove();
                        $('<div id="add_complex">' +
'                            <p><b>Добавление комплекса</b></p>' +
'                            <label for="complex_name">Название:</label>' +
'                            <input id="complex_name" style="display: block;width:100%;">' +
'                            <div id="cmode">' +
'                                <b>Режим установки значений:</b>' +
'                                <label><input type="radio" name="cmode" value="set"><span>Задать</span></label>' +
'                                <label><input type="radio" name="cmode" value="add"><span>Добавить</span></label>' +
'                                <label><input type="radio" name="cmode" value="del"><span>Отнять</span></label>' +
'                            </div>' +
'                        </div>')
                            .appendTo(".popup_footer")
                            .append($("<div class=\"button-design-02 icon_close\">Добавить меню</div>").click(function () {
                                var name = $("#modal_id #complex_name").val();
                                if (name !== "") {
                                    var complex = {
                                        name: name,
                                        mode: $("#modal_id input[name=cmode]:checked").val(),
                                        set: $('#modal_id li input[id*=menu]')
                                            .filter(function () { return (parseInt(this.value) > 0); })
                                            .map(function () { return { id: this.id, val: this.value } })
                                            .toArray()
                                    };
                                    menu.push(complex);
                                    complexToBtn(complex).appendTo("#modal_id .popup_footer");
                                    menu.save();
                                    updateComplexSelector();
                                }
                            }));

                        $($.map(menu, complexToBtn)).each(function () { this.appendTo(".popup_footer") });
                        $("label[for*=complex_] span").click(function () {
                            $("#complex_name").val(this.innerText + " ");
                        });
                        $(".editPupilPopupFooter > *").filter(function () {
                            return (/Сбросить/i).test($(this).text());
                        }).click(function () { $("input.update_pupil_btn").click(); });
                    }
                }
        }, false, true);
    if (obs1 === undefined)
        console.log("err in obs1");
    var obs2 = observ(".page-content .col-75",
        function (m) {
            if ((/^Ведомость/i).test($("#table caption").text())) {
                // управление клавишей alt
                updateComplexSelector();
                for(let i=1;i<4;i++)
                    countPortions(i);
            }
        }, false, true);
    if (obs2 === undefined)
        console.log("err in obs2");
    /******** Конец инициализации **********/
})();
